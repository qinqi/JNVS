package com.onvif.db.service;

import java.util.List;

import com.onvif.db.entity.CameraEntity;

public interface CameraService {
	CameraEntity findOne(CameraEntity cameraEntity);
}
