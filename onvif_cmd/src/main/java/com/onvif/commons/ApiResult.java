package com.onvif.commons;

import java.util.HashMap;


public class ApiResult extends HashMap<String, Object>{
	public ApiResult() {
	}
	public ApiResult(ApiCodeEnum apiCodeEnum) {
		put("code", apiCodeEnum.getCode());
		put("msg", apiCodeEnum.getMsg());
	}
	public ApiResult(ApiCodeEnum apiCodeEnum,Object data) {
		put("code", apiCodeEnum.getCode());
		put("msg", apiCodeEnum.getMsg());
		put("data", data);
	}
	public ApiResult(ApiCodeEnum apiCodeEnum,String msg) {
		put("code", apiCodeEnum.getCode());
		put("msg", msg);
	}
	public ApiResult put(String key,Object value) {
		super.put(key, value);
		return this;
	}

}
