package com.control.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.control.api.Gb28181Api;
import com.control.api.MediaApi;
import com.control.commons.ApiCodeEnum;
import com.control.commons.ApiResult;
import com.control.db.entity.CameraEntity;
import com.control.db.entity.RecordEntity;
import com.control.db.service.CameraService;
import com.control.db.service.RecordService;

@RestController
@RequestMapping("/media/callback")
public class MediaCallbackController {
	@Autowired
	Gb28181Api gb28181Api;
	@Autowired
	MediaApi mediaApi;
	@Autowired
	CameraService cameraService;
	@Autowired
	RecordService recordService;
	@Value("${media.server-ip}")
	String mediaIp;
	@Value("${media.secret}")
	String secret;
	
	/***
	 * 播放器鉴权事件，rtsp/rtmp/http-flv/ws-flv/hls的播放都将触发此鉴权事件
	 * @return
	 
	@RequestMapping("/on_play")
	public ApiResult onPlay(){
		System.out.println("on_play");
		return new ApiResult(ApiCodeEnum.SUCCESS);
	}
	*/
	/***
	 * rtsp/rtmp推流鉴权事件
	 * @return
	 
	@RequestMapping("/on_publish")
	public ApiResult onPublish(){
		System.out.println("on_publish");
		return new ApiResult(ApiCodeEnum.SUCCESS);
	}*/
	/***
	 * 录制mp4完成后通知事件；此事件对回复不敏感。
	 * @return
	 */
	@RequestMapping(value="/on_record_mp4",produces = {"application/json;charset=UTF-8"})
	public ApiResult onRecordMp4(@RequestBody Map<String,Object> params){
		String key = params.get("stream").toString();
		CameraEntity cp = new CameraEntity();
		cp.setLiveKey(key);
		CameraEntity rp = cameraService.findOne(cp);
		if(rp!=null) {
			RecordEntity rec = new RecordEntity();
			rec.setDeviceId(rp.getDeviceId());
			rec.setChannelId(rp.getChannelId());
			rec.setFileName(params.get("file_name").toString());
			rec.setFilePath(params.get("file_path").toString());
			rec.setFolder(params.get("folder").toString());
			rec.setFileSize(Integer.parseInt(params.get("file_size").toString()));
			rec.setStartTime(Integer.parseInt(params.get("start_time").toString()));
			rec.setTimeLen(Integer.parseInt(params.get("time_len").toString()));
			rec.setEndTime(Integer.parseInt(params.get("start_time").toString())+Integer.parseInt(params.get("time_len").toString()));
			rec.setUrl("http://"+mediaIp+"/"+params.get("url").toString()+"?secret="+secret);
			recordService.saveOrUpdate(rec);
		}
		System.out.println("on_record_mp4");
		return new ApiResult(ApiCodeEnum.SUCCESS);
	}
	/***
	 * rtsp/rtmp流注册或注销时触发此事件；此事件对回复不敏感
	 * @return
	 
	@RequestMapping(value="/on_stream_changed",produces = {"application/json;charset=UTF-8"})
	public ApiResult onStreamChanged() {
		System.out.println("on_stream_changed");
		return new ApiResult(ApiCodeEnum.SUCCESS);
	}*/
	/***
	 * 流无人观看时事件，用户可以通过此事件选择是否关闭无人看的流。
	 * @return
	 */
	@RequestMapping(value="/on_stream_none_reader",produces = {"application/json;charset=UTF-8"})
	public ApiResult onStreamNoneReader(@RequestBody Map<String,Object> params) {
		System.out.println("on_stream_none_reader");
		String key = params.get("stream").toString();
		CameraEntity cp = new CameraEntity();
		cp.setLiveKey(key);
		CameraEntity rp = cameraService.findOne(cp);
		if(rp!=null && rp.getType()==1 && rp.getRecordStatus() == 0) {
			Boolean result = gb28181Api.stopLive(rp.getDeviceId(), rp.getChannelId());
			if(result) {
				rp.setLiveStatus(0);
				rp.setLiveKey("");
				cameraService.saveOrUpdate(rp);
				return new ApiResult(ApiCodeEnum.OK);
			} else {
				return new ApiResult(ApiCodeEnum.SERVER_ERROR); 
			}
		}else if(rp!=null && rp.getType()==2 && rp.getRecordStatus() == 0) {
			Boolean onvifLiveResult = mediaApi.delStreamProxy("__defaultVhost__/live/"+rp.getLiveKey());
			if(onvifLiveResult) {
				rp.setLiveStatus(0);
				rp.setLiveKey("");
				cameraService.saveOrUpdate(rp);
				return new ApiResult(ApiCodeEnum.OK);
			} else {
				return new ApiResult(ApiCodeEnum.SERVER_ERROR);
			}
		} else if(rp!=null && rp.getType()==3 && rp.getRecordStatus() == 0) {
			Boolean rRtsp = mediaApi.delStreamProxy("__defaultVhost__/live/"+rp.getLiveKey());
			if(rRtsp) {
				rp.setLiveStatus(0);
				rp.setLiveKey("");
				cameraService.saveOrUpdate(rp);
				return new ApiResult(ApiCodeEnum.OK);
			} else {
				return new ApiResult(ApiCodeEnum.SERVER_ERROR);
			}
		} else {
			return new ApiResult(ApiCodeEnum.INVALID_REQUEST);
		}
	}
	/***
	 * 流未找到事件，用户可以在此事件触发时，立即去拉流，这样可以实现按需拉流；此事件对回复不
	 * @return
	 
	@RequestMapping("/on_stream_not_found")
	public ApiResult onStreamNotFound() {
		System.out.println("on_stream_not_found");
		return new ApiResult(ApiCodeEnum.SUCCESS);
	}
	*/
}
