package com.control.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.control.bean.gb28181.Gb28181ChannelBean;
import com.control.bean.gb28181.Gb28181DeviceBean;
import com.control.db.entity.CameraEntity;
import com.control.db.service.CameraService;

@Component
public class Gb28181Task {
	@Value("${gb28181.server-ip}")
	String gb28181Ip;
	@Value("${gb28181.server-http-port}")
	Integer gb28181HttpPort;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	CameraService cameraService;
	
	/***
	 * 获取国标设备并添加到redis
	 */
	@Scheduled(cron="${gb28181.get-device-corn}")
    private void device(){
		String result = restTemplate.getForObject("http://"+gb28181Ip+":"+gb28181HttpPort+"/gb28181/list", String.class);
		JSONObject jsonObj = JSONObject.parseObject(result);
		JSONArray data = jsonObj.getJSONArray("data");
	    List<Gb28181DeviceBean> deviceList = JSON.parseArray(data.toJSONString(), Gb28181DeviceBean.class);
	    
	    CameraEntity cp = new CameraEntity();
	    cp.setType(1);
	    List<CameraEntity> cameraList = cameraService.listAll(cp);
	    Map<String,CameraEntity> allCameraMap = new HashMap<String, CameraEntity>();
	    for(CameraEntity ac : cameraList) {
	    	allCameraMap.put(ac.getDeviceId()+"_"+ac.getChannelId(), ac);
	    }
    	for(Gb28181DeviceBean gbDevice : deviceList) {
	    	Map<String,Gb28181ChannelBean> gbChannelMap = gbDevice.getChannelMap();
	    	for(Map.Entry<String, Gb28181ChannelBean> m : gbChannelMap.entrySet()){
	    		Gb28181ChannelBean gbChannel = m.getValue();
	    		if(allCameraMap.get(gbDevice.getDeviceId()+"_"+gbChannel.getChannelId())!=null) {
	    			CameraEntity co = allCameraMap.get(gbDevice.getDeviceId()+"_"+gbChannel.getChannelId());
	    			co.setStatus(1);
			    	cameraService.saveOrUpdate(co);
			    	allCameraMap.remove(gbDevice.getDeviceId()+"_"+gbChannel.getChannelId());
	    		} else {
	    			CameraEntity camera = new CameraEntity();
			    	camera.setDeviceId(gbDevice.getDeviceId());
			    	camera.setChannelId(gbChannel.getChannelId());
			    	camera.setType(1);
			    	camera.setStatus(1);
			    	camera.setLiveStatus(0);
			    	camera.setRecordStatus(0);
			    	camera.setLiveKey("");
			    	cameraService.saveOrUpdate(camera);
	    		}
	    	}
	    }
    	//其余掉线
    	for(Map.Entry<String, CameraEntity> entry : allCameraMap.entrySet()){
    	    CameraEntity mapValue = entry.getValue();
    	    mapValue.setStatus(0);
    	    cameraService.saveOrUpdate(mapValue);
    	}
	}
}
